let grilleActeurs = document.querySelector('.grilleActeurs')

async function movieCredits (lien){
   return $.ajax({
        type: 'GET',
        url: lien,
        dataType: 'json',
    })
}

movieCredits('https://api.themoviedb.org/3/movie/438631/credits?api_key=c3abcef9cf5d761549f21bc2a7781714').then(response => {
    let cast=response['cast']
    for(let i=0; i<15; i++){
        let acteur = cast[i];
        let imgActeur = document.createElement('img');
        imgActeur.src = 'https://image.tmdb.org/t/p/w500' + acteur['profile_path'];
        let nomActeur = document.createElement('h3');
        nomActeur.textContent = acteur['name'];
        nomActeur.classList.add('nomActeur');
        let carteActeur = document.createElement('div');
        carteActeur.classList.add('carteActeur');
        carteActeur.id=acteur['id'];
        carteActeur.appendChild(imgActeur);
        carteActeur.appendChild(nomActeur);
        console.log(carteActeur);
        grilleActeurs.appendChild(carteActeur);
    }
})