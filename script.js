// CARR ACTEURS

// Variable globale
let indexAct = 0;

let nomAff = document.querySelector('.acteurs>.bande>.nom')
let noms = [
    "Timothée Chalamet as Paul Atreïdes",
    "Rebecca Ferguson as Lady Jessica",
    "Oscar Isaac as Duke Leto Atreïdes",
    "Jason Momo as Duncan Idaho",
    "Josh Brolin as Gurney Halleck",
    "Chang Chen as Dr.Wellington Yueh",
    "Zendaya as Chani",
    "Javier Bardem as Stilgar",
    "Sharon Duncan - Brewster as Liet Kynes",
    "Stellan Skarsgard as Baron Harkonnen",
    "Dave Bautista as Rabban Harkonnen",
]

// Gestion des événements
$('.carrActeurs>div>.fleche.prec').click(function () {
    // Récupération index
    let indexAN = indexAct - 1
    if (indexAN < 0) {
        indexAN = 10
    }

    // Renouveller l'image
    $('.carrActeurs>div>img').eq(indexAct).fadeOut(1000).end().eq(indexAN).fadeIn(1000);
    nomAff.textContent = noms[indexAN]

    // Mettre à jour l'index
    indexAct = indexAN;
    console.log(indexAct)
});

$('.carrActeurs>div>.fleche.suiv').click(function () {
    // Récupération index
    let indexAN = indexAct + 1
    if (indexAN > 10) {
        indexAN = 0
    }

    // Renouveller l'image
    $('.carrActeurs>div>img').eq(indexAct).fadeOut(1000).end().eq(indexAN).fadeIn(1000);
    nomAff.textContent = noms[indexAN]

    // Mettre à jour l'index
    indexAct = indexAN;
    console.log(indexAct)
});
// FIN CARR ACTEURS

//CARR IMG

let indexImg = 0;

// Gestion des événements
$('.carrImg>div>.fleche.prec').click(function () {
    // Récupération index
    let indexIN = indexImg - 1
    if (indexIN < 0) {
        indexIN = 8
    }

    // Renouveller l'image
    $('.carrImg>div>img').eq(indexImg).fadeOut(1000).end().eq(indexIN).fadeIn(1000);
    nomAff.textContent = noms[indexIN]

    // Mettre à jour l'index
    indexImg = indexIN;
    console.log(indexImg)
});

$('.carrImg>div>.fleche.suiv').click(function () {
    // Récupération index
    let indexIN = indexImg + 1
    if (indexIN > 8) {
        indexIN = 0
    }

    // Renouveller l'image
    $('.carrImg>div>img').eq(indexImg).fadeOut(1000).end().eq(indexIN).fadeIn(1000);
    nomAff.textContent = noms[indexIN]

    // Mettre à jour l'index
    indexImg = indexIN;
    console.log(indexImg)
});

//FIN CARR IMG


// MAP

var map = L.map('map').setView([48.8566, 2.3522], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


fetch('./cinema_fr.geojson')
    .then((response) => response.json())
    .then((json) => {
        // Créez une couche GeoJSON et ajoutez-la à la carte
        var cinemasLayer = L.geoJSON(json).addTo(map);

        // Ajoutez une popup à chaque marqueur (épingle) dans la couche GeoJSON
        cinemasLayer.eachLayer(function (layer) {
            var properties = layer.feature.properties;
            var popupContent = `
                <strong>Nom du cinéma:</strong> ${properties.NOM_ETABLISSEMENT}<br>
                <strong>Commune:</strong> ${properties.COMMUNE}<br>
                <strong>Salles:</strong> ${properties.ECRANS}
            `;
            layer.bindPopup(popupContent);
        });
    });

document.querySelector('.geoBtn').addEventListener('click', function () {
    // Vérifiez si la géolocalisation est disponible dans le navigateur
    if ("geolocation" in navigator) {
        // Demandez la position de l'utilisateur
        navigator.geolocation.getCurrentPosition(function (position) {
            var userLatitude = position.coords.latitude;
            var userLongitude = position.coords.longitude;

            // Centrez la carte sur la position de l'utilisateur
            map.setView([userLatitude, userLongitude], 13);
        }, function (error) {
            // En cas d'erreur lors de la géolocalisation
            console.error('Erreur de géolocalisation : ' + error.message);
        });
    } else {
        // Si la géolocalisation n'est pas disponible dans le navigateur
        alert('La géolocalisation n\'est pas prise en charge dans votre navigateur.');
    }
});
